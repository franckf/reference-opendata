package main

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esutil"
)

type statsFile struct {
	name   string
	succes uint64
	failed uint64
}

func main() {
	start := time.Now().UTC()

	cert, _ := ioutil.ReadFile("ca.crt")
	cfg := elasticsearch.Config{
		Addresses:         []string{"https://localhost:9200"},
		Username:          "elastic",
		Password:          "3lastic",
		EnableDebugLogger: true,
		CACert:            cert,
	}

	es, err := elasticsearch.NewClient(cfg)
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	var filesNames []string
	err = filepath.WalkDir("datas", func(path string, info fs.DirEntry, err error) error {
		if err != nil {
			fmt.Println(err)
			return err
		}
		if !info.IsDir() {
			filename := strings.Split(path, "/")[1]
			filesNames = append(filesNames, filename)
		}
		return nil
	})
	if err != nil {
		fmt.Println(err)
	}

	var statsFiles []statsFile

	for _, fileName := range filesNames {
		file, err := os.Open("datas/" + fileName)
		if err != nil {
			log.Fatalln("Opening file ", err)
		}
		defer file.Close()

		bi, err := esutil.NewBulkIndexer(esutil.BulkIndexerConfig{
			Index:  "energy-" + fileName,
			Client: es,
		})
		if err != nil {
			log.Fatalf("Error creating the indexer: %s", err)
		}

		readFile(bi, file)

		if err := bi.Close(context.Background()); err != nil {
			log.Fatalf("Unexpected error: %s", err)
		}

		biStats := bi.Stats()
		statsFiles = append(statsFiles, statsFile{fileName,
			biStats.NumFlushed,
			biStats.NumFailed,
		})
	}

	dur := time.Since(start).Truncate(time.Millisecond)
	log.Printf("time.Since(start): %v\n", dur)
	fmt.Println(strings.Repeat("-", 35))
	fmt.Println("name", "\t", "succes", "\t", "failed")
	for _, v := range statsFiles {
		fmt.Println(v.name, "\t", v.succes, "\t", v.failed)
	}
}

func readFile(es esutil.BulkIndexer, file io.ReadWriter) {
	scanner := bufio.NewScanner(file)
	var count int
	var header []string

	for scanner.Scan() {
		currentLine := scanner.Text()
		parsedLine := strings.Split(currentLine, ";")
		if count == 0 {
			header = parsedLine
			count++
			continue
		}
		record := parsedLine
		if len(header) != len(record) {
			// log.Printf("Error while parsing for line %d - %#+v", count+1, record)
			count++
			continue
		}
		var buf bytes.Buffer
		buf.WriteString(`{`)
		for i, _ := range header {
			buf.WriteString(`"`)
			buf.WriteString(header[i])
			buf.WriteString(`":"`)
			buf.WriteString(record[i])
			buf.WriteString(`"`)
			if i != len(header)-1 {
				buf.WriteString(`,`)
			}
		}
		buf.WriteString(`}`)

		err := es.Add(
			context.Background(),
			esutil.BulkIndexerItem{
				Action: "index",
				Body:   bytes.NewReader(buf.Bytes()),
				OnSuccess: func(
					ctx context.Context,
					item esutil.BulkIndexerItem,
					res esutil.BulkIndexerResponseItem,
				) {
					// log.Printf("[%d] %s %s", res.Status, res.Result, item.DocumentID)
				},
				OnFailure: func(
					ctx context.Context,
					item esutil.BulkIndexerItem,
					res esutil.BulkIndexerResponseItem,
					err error) {
					/*
						if err != nil {
							log.Printf("ERROR: %s", err)
						} else {
							log.Printf("ERROR: %s: %s", res.Error.Type, res.Error.Reason)
						}
					*/
				},
			},
		)
		if err != nil {
			log.Fatalf("Unexpected error: %s", err)
		}
		count++
	}
}
