package main

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esutil"
)

func main() {
	start := time.Now().UTC()

	mappingBasseTensions := `
{
"mappings": {
"properties": {
"Geo Point":               { "type": "geo_point" },
"Geo Shape":               { "type": "geo_shape" },
"POSITIO":                 { "type": "text" },
"TENSION":                 { "type": "text" },
"NOM_GRD":                 { "type": "text" },
"code_insee":              { "type": "integer" }
              }
            }
}
`

	// failed to parse geoshape
	// csv column by column is not clean for parsing
	// need clean struct with json but it break a general purpose indexer
	// unless we cerate a proper lib
	indexing("lignes-electriques-souterraines-basse-tension-bt-sample.csv", mappingBasseTensions)
	fmt.Printf("time.Since(start): %v\n", time.Since(start).Truncate(time.Millisecond))
	fmt.Println(strings.Repeat("-", 35))
}

func indexing(fileName string, mapping string) {
	indexName := "energy-" + fileName

	cert, _ := ioutil.ReadFile("ca.crt")
	cfg := elasticsearch.Config{
		Addresses:         []string{"https://localhost:9200"},
		Username:          "elastic",
		Password:          "3lastic",
		EnableDebugLogger: true,
		CACert:            cert,
	}

	es, err := elasticsearch.NewClient(cfg)
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	file, err := os.Open("datas/" + fileName)
	if err != nil {
		log.Fatalln("Opening file ", err)
	}
	defer file.Close()

	res, err := es.Indices.Exists([]string{indexName})
	if err != nil {
		log.Fatalf("error: Indices.Exists: %s", err)
	}
	res.Body.Close()
	if res.StatusCode == 404 {
		res, err := es.Indices.Create(
			indexName,
			es.Indices.Create.WithBody(strings.NewReader(mapping)),
		)
		if err != nil {
			log.Fatalf("error: Indices.Create: %s", err)
		}
		if res.IsError() {
			log.Fatalf("error: Indices.Create: %s", res)
		}
	}

	bi, err := esutil.NewBulkIndexer(esutil.BulkIndexerConfig{
		Index:  indexName,
		Client: es,
	})
	if err != nil {
		log.Fatalf("Error creating the indexer: %s", err)
	}

	readFile(bi, file)

	if err := bi.Close(context.Background()); err != nil {
		log.Fatalf("Unexpected error: %s", err)
	}

	biStats := bi.Stats()
	fmt.Println("succes:", "\t", biStats.NumFlushed)
	fmt.Println("failed:", "\t", biStats.NumFailed)

}

func readFile(es esutil.BulkIndexer, file io.ReadWriter) {
	scanner := bufio.NewScanner(file)
	var count int
	var header []string

	for scanner.Scan() {
		currentLine := scanner.Text()
		parsedLine := strings.Split(currentLine, ";")
		if count == 0 {
			header = parsedLine
			count++
			continue
		}
		record := parsedLine
		if len(header) != len(record) {
			log.Printf("Error while parsing for line %d - %#+v", count+1, record)
			count++
			continue
		}
		var buf bytes.Buffer
		buf.WriteString(`{`)
		for i, _ := range header {
			buf.WriteString(`"`)
			buf.WriteString(header[i])
			buf.WriteString(`":"`)
			buf.WriteString(record[i])
			buf.WriteString(`"`)
			if i != len(header)-1 {
				buf.WriteString(`,`)
			}
		}
		buf.WriteString(`}`)

		err := es.Add(
			context.Background(),
			esutil.BulkIndexerItem{
				Action: "index",
				Body:   bytes.NewReader(buf.Bytes()),
				OnSuccess: func(
					ctx context.Context,
					item esutil.BulkIndexerItem,
					res esutil.BulkIndexerResponseItem,
				) {
					// log.Printf("[%d] %s %s", res.Status, res.Result, item.DocumentID)
				},
				OnFailure: func(
					ctx context.Context,
					item esutil.BulkIndexerItem,
					res esutil.BulkIndexerResponseItem,
					err error) {

					if err != nil {
						log.Printf("ERROR: %s", err)
					} else {
						log.Printf("ERROR: %s: %s", res.Error.Type, res.Error.Reason)
					}

				},
			},
		)
		if err != nil {
			log.Fatalf("Unexpected error: %s", err)
		}
		count++
	}
}
