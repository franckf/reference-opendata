# Regroupement et recroisement des informations publiques sur l'énergie dans la vienne

## Sources

- https://data.opendatasoft.com/explore/dataset/lignes-electriques-souterraines-basse-tension-bt%40grandpoitiers/api/
- https://data.opendatasoft.com/explore/dataset/registre-francais-des-emissions-polluantes-emissions%40grandpoitiers/api/
- https://data.opendatasoft.com/explore/dataset/proprete-reseau-de-chauffage-urbain-grand-poitiers-donnees-metiers%40grandpoitiers/api/
- https://data.opendatasoft.com/explore/dataset/sde_consommation_energie_primaire%40grandpoitiers/api/
- https://opendata.soregies.fr/explore/dataset/reseaux-gaz-naturel-departement-de-la-vienne-86/information/
- https://opendata.soregies.fr/explore/dataset/reseaux-gaz-propane-departement-de-la-vienne-86/api/

### Datajoule

- https://data.opendatasoft.com/explore/dataset/communes-gaz%40akajoule/table/?disjunctive.nom_reg&sort=population
- https://data.opendatasoft.com/explore/dataset/points-dinjection-de-biomethane-en-france@akajoule/
- https://data.opendatasoft.com/explore/dataset/suivi-production-pv-particulier%40akajoule/table/
- https://data.opendatasoft.com/explore/dataset/stations-gnv-publiques-en-france%40akajoule/table/?disjunctive.nom_region
- https://data.opendatasoft.com/explore/dataset/suivi-cartes-carburant-akajoule%40akajoule/table/?disjunctive.designation_produit&disjunctive.immatriculation&sort=date
- https://data.opendatasoft.com/explore/dataset/demonstration-courbe-de-charge-linky%40akajoule/table/?sort=-horodatage_debut
- https://data.opendatasoft.com/explore/dataset/intercommunalites-2019-france%40akajoule/table/?disjunctive.reg_name&disjunctive.dep_name&disjunctive.epci_name&sort=year
- https://data.opendatasoft.com/explore/dataset/prix-des-energies-en-france%40akajoule/export/
- https://data.opendatasoft.com/explore/dataset/consommation-par-region-par-energie%40akajoule/table/?disjunctive.region
- https://data.opendatasoft.com/explore/dataset/consommation-par-region-par-energie-et-par-secteur%40akajoule/table/?disjunctive.region
- https://data.opendatasoft.com/explore/dataset/population-2019-communes-france%40akajoule/table/?disjunctive.epci_name&sort=com_arm_name
- https://data.opendatasoft.com/explore/dataset/consommation-par-region-par-secteur%40akajoule/table/?disjunctive.region
- https://data.opendatasoft.com/explore/dataset/bornes-de-recharge-pour-vehicules-electriques-irve%40akajoule/table/
- https://data.opendatasoft.com/explore/dataset/historique-des-tarifs-dachats-photovoltaique%40akajoule/export/
- https://data.opendatasoft.com/explore/dataset/donnees-geographiques-generalisees-descriptives-du-trace-du-reseau-de-grtgaz-au-%40akajoule/table/
- https://data.opendatasoft.com/explore/dataset/communes-2019-france%40akajoule/table/?disjunctive.reg_name&disjunctive.dep_name&disjunctive.arrdep_name&disjunctive.ze2020_name&disjunctive.bv2012_name&disjunctive.epci_name&disjunctive.ept_name&disjunctive.com_name&disjunctive.ze2010_name&disjunctive.com_is_mountain_area&sort=year
- https://data.opendatasoft.com/explore/dataset/production-electrique-annuelle-par-filiere-a-la-maille-commune%40akajoule/table/?sort=annee
- https://data.opendatasoft.com/explore/dataset/parc-national-annuel-de-production-eolien-et-solaire-2001-a-2020%40akajoule/table/?sort=annee
- https://data.opendatasoft.com/explore/dataset/courbe-de-charge-linky-bertrand%40akajoule/table/
- https://data.opendatasoft.com/explore/dataset/importations-de-gaz-naturel-france-2020%40akajoule/table/
- https://data.opendatasoft.com/explore/dataset/populations-legales-communes-et-arrondissements-municipaux-2019-france%40akajoule/table/?disjunctive.reg_code&disjunctive.reg_name&disjunctive.dep_code&disjunctive.arrdep_code&disjunctive.com_arm_code&disjunctive.com_arm_name&disjunctive.epci_name&disjunctive.epci_code&disjunctive.dep_name

## Ingests

Try to use api:
```
https://data.opendatasoft.com/api/records/1.0/search/?dataset=lignes-electriques-souterraines-basse-tension-bt%40grandpoitiers
```
```
{"nhits": 6853849, "parameters": {"dataset": ["lignes-electriques-souterraines-basse-tension-bt@grandpoitiers"], "rows": 10, "start": 0, "format": "json", "timezone": "UTC"}, "records": [{"datasetid": "lignes-electriques-souterraines-basse-tension-bt@grandpoitiers", "recordid": "8ee2e608d95b94b394a67e579926e314f7f5f59a", "fields": {"nom_grd": "Enedis", "geo_shape": {"coordinates": [[3.066815679448522, 44.10908746415647], [3.066820977399905, 44.109085081948244], [3.067023429751729, 44.10899416365147], [3.067084785691891, 44.10890375729343], [3.066534050415751, 44.10881726408093], [3.066451161242132, 44.10877539618557], [3.066446048971071, 44.108772813825645]], "type": "LineString"}, "tension": "BT", "positio": "souterrain", "geo_point_2d": [44.10890509038709, 3.0668298903387363], "code_insee": "12145"}, "geometry": {"type": "Point", "coordinates": [3.0668298903387363, 44.10890509038709]}, "record_timestamp": "2021-03-25T12:02:10.479Z"}, {"datasetid":
```
But calls are limited to 10k:
```
"error": "The sum of `start` + `rows` parameters can not be more than 10000. Please refine your query or use the Download service."
```
So we will use ~~json or~~ csv file.

**Problem** we have a lot of datasets, ingest them all is easy but we also need to parse them to extract mapping and identify geopoint fields.  
So :
- open a file
- read header
- extract mapping
- create index+mapping
- insert first line and test mapping ?
- index all lines
- repeat for all files

> Use all files, is a mess and prone to errors, a lot of lines are lost and elastic may crash. As painfull it is all files must be maps.

