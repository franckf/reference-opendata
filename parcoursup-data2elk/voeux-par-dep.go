package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/elastic/go-elasticsearch/v7"
)

func voeByDepSQL() {
	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	body := fmt.Sprintf(`{"query":"\nSELECT dep,voe_tot FROM \"%s\"\n  "}`, index)
	res, err := es.SQL.Query(
		strings.NewReader(body),
	)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		var resError esError
		if err := json.NewDecoder(res.Body).Decode(&resError); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		} else {
			log.Fatalf("[%s] %s: %s",
				res.Status(),
				resError.Error.RootCause[0].Type,
				resError.Error.RootCause[0].Reason,
			)
		}
	}

	var dataRes sqlRes
	if err := json.NewDecoder(res.Body).Decode(&dataRes); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	defer res.Body.Close()

	var voeByDep = make(map[int]int, 0)
	for i := 0; i < len(dataRes.Rows); i++ {
		curDep, _ := strconv.Atoi(dataRes.Rows[i][0])
		curVoe, _ := strconv.Atoi(dataRes.Rows[i][1])
		oldVoe := voeByDep[curDep]
		voeByDep[curDep] = oldVoe + curVoe
	}
	log.Printf("voeByDep: %#+v\n", voeByDep)
}

type sqlRes struct {
	Columns []Columns  `json:"columns"`
	Rows    [][]string `json:"rows"`
}

type Columns struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

func voeByDepSQLWithoutClient() {
	body := fmt.Sprintf(`{"query":"\nSELECT dep,voe_tot FROM \"%s\"\n  "}`, index)
	resp, err := http.Post("http://localhost:9200/_sql", "application/json", strings.NewReader(body))
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	bodyRes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("bodyRes: %#+v\n", string(bodyRes))

	var parseBody sqlRes
	err = json.Unmarshal(bodyRes, &parseBody)
	if err != nil {
		log.Fatal(err)
	}

	var voeByDep = make(map[int]int, 0)
	for i := 0; i < len(parseBody.Rows); i++ {
		curDep, _ := strconv.Atoi(parseBody.Rows[i][0])
		curVoe, _ := strconv.Atoi(parseBody.Rows[i][1])
		oldVoe := voeByDep[curDep]
		voeByDep[curDep] = oldVoe + curVoe
	}
	log.Printf("voeByDep: %#+v\n", voeByDep)
}

func voeByDepBody() {
	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	body := `{"_source": ["dep", "voe_tot"]}`

	res, err := es.Search(
		es.Search.WithIndex(index),
		es.Search.WithBody(strings.NewReader(body)),
		es.Search.WithTrackTotalHits(true),
		es.Search.WithSize(14000), // need scroll to have more than 10k - for now I cheat by changing max_result_window in index settings
	)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		var resError esError
		if err := json.NewDecoder(res.Body).Decode(&resError); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		} else {
			log.Fatalf("[%s] %s: %s",
				res.Status(),
				resError.Error.RootCause[0].Type,
				resError.Error.RootCause[0].Reason,
			)
		}
	}

	var dataRes esRes
	if err := json.NewDecoder(res.Body).Decode(&dataRes); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	defer res.Body.Close()
	log.Printf(
		"[%s]; %d hits; took: %dms",
		res.Status(),
		dataRes.Hits.Total.Value,
		dataRes.Took,
	)

	log.Printf("len(dataRes.Hits.Hits): %#+v\n", len(dataRes.Hits.Hits))
	log.Printf("data: %#+v - %#+v \n", dataRes.Hits.Hits[0].Source.Dept, dataRes.Hits.Hits[0].Source.Voeux)

	var voeByDep = make(map[int]int, 0)
	for i := 0; i < len(dataRes.Hits.Hits); i++ {
		curDep, _ := strconv.Atoi(dataRes.Hits.Hits[i].Source.Dept)
		curVoe, _ := strconv.Atoi(dataRes.Hits.Hits[i].Source.Voeux)
		oldVoe := voeByDep[curDep]
		voeByDep[curDep] = oldVoe + curVoe
	}

	log.Printf("voeByDep: %#+v\n", voeByDep)
}

// can't find a way to have columns results with lucene api
func voeByDepQuery() {

	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	lucene := `dep : 75 AND fili : BTS`

	res, err := es.Search(
		es.Search.WithIndex(index),
		es.Search.WithQuery(lucene),
		es.Search.WithTrackTotalHits(true),
		es.Search.WithSize(14000), // need scroll to have more than 10k - for now I cheat by changing max_result_window in index settings
	)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		var resError esError
		if err := json.NewDecoder(res.Body).Decode(&resError); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		} else {
			log.Fatalf("[%s] %s: %s",
				res.Status(),
				resError.Error.RootCause[0].Type,
				resError.Error.RootCause[0].Reason,
			)
		}
	}

	var dataRes esRes
	if err := json.NewDecoder(res.Body).Decode(&dataRes); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	defer res.Body.Close()
	log.Printf(
		"[%s]; %d hits; took: %dms",
		res.Status(),
		dataRes.Hits.Total.Value,
		dataRes.Took,
	)

	log.Printf("len(dataRes.Hits.Hits): %#+v\n", len(dataRes.Hits.Hits))
	log.Printf("data: %#+v - %#+v \n", dataRes.Hits.Hits[0].Source.CodeUai, dataRes.Hits.Hits[0].Source.Voeux)

	var voeByDep = make(map[int]int, 0)
	for i := 0; i < len(dataRes.Hits.Hits); i++ {
		curDep, _ := strconv.Atoi(dataRes.Hits.Hits[i].Source.Dept)
		curVoe, _ := strconv.Atoi(dataRes.Hits.Hits[i].Source.Voeux)
		oldVoe := voeByDep[curDep]
		voeByDep[curDep] = oldVoe + curVoe
	}

	log.Printf("voeByDep: %#+v\n", voeByDep)
}
