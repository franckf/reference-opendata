package openfiles

import (
	"reflect"
	"testing"
)

func TestFieldsCSV(t *testing.T) {
	type args struct {
		line string
	}
	tests := []struct {
		name       string
		args       args
		wantFields []string
	}{
		{
			name: "one liner",
			args: args{
				line: ("field1;field2"),
			},
			wantFields: []string{"field1", "field2"},
		},
		{
			name: "double quotes",
			args: args{
				line: (`"field1";"field2";"field3"`),
			},
			wantFields: []string{"field1", "field2", "field3"},
		},
		{
			name: "comma sep",
			args: args{
				line: (`field1,field2,field3`),
			},
			wantFields: []string{"field1", "field2", "field3"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotFields := ExtractFieldsFromLine(tt.args.line); !reflect.DeepEqual(gotFields, tt.wantFields) {
				t.Errorf("ExtractFieldsFromLine() = %v, want %v", gotFields, tt.wantFields)
			}
		})
	}
}
