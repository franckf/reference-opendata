package openfiles

import (
	"strings"
)

func ExtractFieldsFromLine(line string) (fields []string) {
	fields = strings.Split(line, ";")
	if len(fields) == 1 {
		fields = strings.Split(line, ",")
	}
	for i, field := range fields {
		fields[i] = strings.Trim(field, `"`)
	}
	return
}
