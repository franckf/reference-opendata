package bulking

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esutil"
	"gitlab.com/franckf/reference-opendata/parcoursup-data2elk/indexing"
	"gitlab.com/franckf/reference-opendata/parcoursup-data2elk/mappings"
	"gitlab.com/franckf/reference-opendata/parcoursup-data2elk/openfiles"
)

func Csv(file io.Reader, index string) (err error) {
	start := time.Now().UTC()

	esClient, err := elasticsearch.NewDefaultClient()
	if err != nil {
		err = fmt.Errorf("error creating the client: %s", err)
		return
	}

	mappings.ParcoursSup(esClient, index, mappings.ParcoursSupFields)

	err = mappings.CreateIndexPatternKibana(index)
	if err != nil {
		err = fmt.Errorf("error while creating index pattern: %s", err)
		return
	}

	bulkIndexer, err := esutil.NewBulkIndexer(esutil.BulkIndexerConfig{
		NumWorkers:    4,
		FlushBytes:    int(5e+6),
		FlushInterval: 30 * time.Second,
		Client:        esClient,
		Index:         index,
	})
	if err != nil {
		err = fmt.Errorf("error creating the indexer: %s", err)
		return
	}

	resD, err := esClient.Indices.Delete([]string{index}, esClient.Indices.Delete.WithIgnoreUnavailable(true))
	if err != nil || resD.IsError() {
		err = fmt.Errorf("cannot delete index: %s", err)
		return
	}
	resD.Body.Close()

	resC, err := esClient.Indices.Create(index)
	if err != nil || resC.IsError() {
		err = fmt.Errorf("cannot create index %s: %s", resC, err)
		return
	}
	resC.Body.Close()

	scanner := bufio.NewScanner(file) // max buffer: 64*1024
	var firstLine string
	for scanner.Scan() {
		firstLine = scanner.Text()
		break
	}

	titlesFields := openfiles.ExtractFieldsFromLine(firstLine)
	var currentLine string

	for scanner.Scan() {
		currentLine = scanner.Text()
		currentFields := openfiles.ExtractFieldsFromLine(currentLine)

		bodyIndex := indexing.FormatingIndexBody(titlesFields, currentFields)

		// how to not index data already existing ? duplicate aren't reinsert
		// very slow ? much faster than pure index
		err = bulkIndexer.Add(
			context.Background(),
			esutil.BulkIndexerItem{
				// Action field configures the operation to perform (index, create, delete, update)
				Action: "index",
				// DocumentID is the (optional) document ID
				//DocumentID: strconv.Itoa(a.ID),
				// Body is an `io.Reader` with the payload
				Body: bodyIndex,
				// OnSuccess is called for each successful operation
				// OnSuccess: func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem) {
				// 	println("insert")
				// },
				// OnFailure is called for each failed operation
				OnFailure: func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem, err error) {
					if err != nil {
						log.Printf("ERROR: %s", err)
					} else {
						log.Printf("ERROR: %s: %s", res.Error.Type, res.Error.Reason)
					}
				},
			},
		)
		if err != nil {
			log.Fatalf("Unexpected error: %s", err)
		}
	}

	err = scanner.Err()
	if err != nil {
		err = fmt.Errorf("error while scanning file: %s", err)
		return
	}

	if err := bulkIndexer.Close(context.Background()); err != nil {
		log.Fatalf("Unexpected error: %s", err)
	}

	biStats := bulkIndexer.Stats()
	dur := time.Since(start)
	log.Printf("bulk index is over: %v\n %v\n", dur.Seconds(), biStats.NumAdded)
	return
}
