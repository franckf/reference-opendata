package indexing

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"github.com/elastic/go-elasticsearch/v7"
	"gitlab.com/franckf/reference-opendata/parcoursup-data2elk/mappings"
)

type JsonPayLoad struct {
	Fields FieldS `json:"fields"`
}

type FieldS struct {
	Lien_form_psup string `json:"lien_form_psup"`
}

// JsonStructur is working poc but not complete
func JsonStructur(file io.Reader, index string) (err error) {

	esClient, err := elasticsearch.NewDefaultClient()
	if err != nil {
		err = fmt.Errorf("error creating the client: %s", err)
		return
	}

	mappings.ParcoursSup(esClient, index, mappings.ParcoursSupFields)

	err = mappings.CreateIndexPatternKibana(index)
	if err != nil {
		err = fmt.Errorf("error while creating index pattern: %s", err)
		return
	}

	fileBuffer := new(bytes.Buffer)
	fileBuffer.ReadFrom(file) // put file content in mem - not great but it is a big one liner

	var payloadStructure []JsonPayLoad
	err = json.Unmarshal(fileBuffer.Bytes(), &payloadStructure)
	if err != nil {
		err = fmt.Errorf("error while unmarshal json: %s", err)
		return
	}

	for _, document := range payloadStructure {
		res, errIndex := esClient.Index(
			index,
			strings.NewReader(`{"lien_form_psup":"`+document.Fields.Lien_form_psup+`"}`), // only one field - need to structure all to have it all
		)
		res.Body.Close()
		if errIndex != nil {
			err = fmt.Errorf("error while indexing line: %s", err)
			return
		}
	}

	return
}

// JsonUnstructur is working poc but not complete
func JsonUnstructur(file io.Reader, index string) (err error) {

	esClient, err := elasticsearch.NewDefaultClient()
	if err != nil {
		err = fmt.Errorf("error creating the client: %s", err)
		return
	}

	mappings.ParcoursSup(esClient, index, mappings.ParcoursSupFields)

	err = mappings.CreateIndexPatternKibana(index)
	if err != nil {
		err = fmt.Errorf("error while creating index pattern: %s", err)
		return
	}

	fileBuffer := new(bytes.Buffer)
	fileBuffer.ReadFrom(file) // put file content in mem - not great but it is a big one liner
	var payloadUnstructure []map[string]interface{}
	err = json.Unmarshal(fileBuffer.Bytes(), &payloadUnstructure)
	if err != nil {
		err = fmt.Errorf("error while unmarshal json: %s", err)
		return
	}

	for _, document := range payloadUnstructure {
		res, errIndex := esClient.Index(
			index,
			strings.NewReader(`{"fil_lib_voe_acc":"`+document["fields"].(map[string]interface{})["fil_lib_voe_acc"].(string)+`"}`), // only one field, - need to list them all to index
		)
		res.Body.Close()
		if errIndex != nil {
			err = fmt.Errorf("error while indexing line: %s", err)
			return
		}
	}

	return
}
