package indexing

import (
	"bufio"
	"fmt"
	"io"
	"strings"

	"github.com/elastic/go-elasticsearch/v7"
	"gitlab.com/franckf/reference-opendata/parcoursup-data2elk/mappings"
	"gitlab.com/franckf/reference-opendata/parcoursup-data2elk/openfiles"
)

func Csv(file io.Reader, index string) (err error) {

	esClient, err := elasticsearch.NewDefaultClient()
	if err != nil {
		err = fmt.Errorf("error creating the client: %s", err)
		return
	}
	// log.Println(esClient.Info())

	mappings.ParcoursSup(esClient, index, mappings.ParcoursSupFields)

	err = mappings.CreateIndexPatternKibana(index)
	if err != nil {
		err = fmt.Errorf("error while creating index pattern: %s", err)
		return
	}

	scanner := bufio.NewScanner(file) // max buffer: 64*1024
	var firstLine string
	for scanner.Scan() {
		firstLine = scanner.Text()
		break
	}

	titlesFields := openfiles.ExtractFieldsFromLine(firstLine)
	var currentLine string

	for scanner.Scan() {
		currentLine = scanner.Text()
		currentFields := openfiles.ExtractFieldsFromLine(currentLine)

		bodyIndex := FormatingIndexBody(titlesFields, currentFields)

		// how to not index data already existing ?
		// very slow
		res, errIndex := esClient.Index(
			index,
			bodyIndex,
		)
		res.Body.Close()
		if errIndex != nil {
			err = fmt.Errorf("error while indexing line: %s", err)
			return
		}
	}

	err = scanner.Err()
	if err != nil {
		err = fmt.Errorf("error while scanning file: %s", err)
		return
	}

	return
}

func FormatingIndexBody(titles []string, fields []string) (body io.Reader) {
	var bodyString string
	if len(titles) == 0 || len(fields) == 0 {
		fmt.Println("missing titles or fileds, while not be imported")
		body = strings.NewReader(bodyString)
		return
	}
	var fieldsN []string
	var titlesN []string
	switch {
	case len(titles) > len(fields):
		fieldsN = make([]string, len(titles))
		titlesN = make([]string, len(titles))
	case len(titles) <= len(fields):
		fieldsN = make([]string, len(fields))
		titlesN = make([]string, len(fields))
	}
	copy(fieldsN, fields)
	copy(titlesN, titles)
	bodyString = (`{`)
	for i := range titlesN {
		if titlesN[i] == "" {
			titlesN[i] = "unknown"
		}
		bodyString += `"` + titlesN[i] + `":"` + fieldsN[i] + `"`
		if i != len(titlesN)-1 {
			bodyString += `,`
		}
	}
	bodyString += `}`
	body = strings.NewReader(bodyString)
	return
}
