package indexing

import (
	"io"
	"reflect"
	"strings"
	"testing"
)

func Test_formatingIndexBody(t *testing.T) {
	type args struct {
		titles []string
		fields []string
	}
	tests := []struct {
		name     string
		args     args
		wantBody io.Reader
	}{
		{
			name: "basic formating",
			args: args{
				titles: []string{"field1", "field2"},
				fields: []string{"line1", "line2"},
			},
			wantBody: strings.NewReader(`{"field1":"line1","field2":"line2"}`),
		},
		{
			name: "too much titles",
			args: args{
				titles: []string{"field1", "field2", "field3"},
				fields: []string{"line1", "line2"},
			},
			wantBody: strings.NewReader(`{"field1":"line1","field2":"line2","field3":""}`),
		},
		{
			name: "too much fields",
			args: args{
				titles: []string{"field1", "field2"},
				fields: []string{"line1", "line2", "line3"},
			},
			wantBody: strings.NewReader(`{"field1":"line1","field2":"line2","unknown":"line3"}`),
		},
		{
			name: "missing titles",
			args: args{
				titles: []string{},
				fields: []string{"line1", "line2"},
			},
			wantBody: strings.NewReader(``),
		},
		{
			name: "missing fields",
			args: args{
				titles: []string{"field1", "field2"},
				fields: []string{},
			},
			wantBody: strings.NewReader(``),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotBody := FormatingIndexBody(tt.args.titles, tt.args.fields); !reflect.DeepEqual(gotBody, tt.wantBody) {
				t.Errorf("formatingIndexBody() = %v, want %v", gotBody, tt.wantBody)
			}
		})
	}
}
