# parcoursup

From the data of [parcoursup](https://fr.wikipedia.org/wiki/Parcoursup) 2021, tests the import of differents formats to Elasticsearch. And visualize them with Kibana.

Don't worry about data persistancy in database or in the index of the containers, focus on imports.

> goal reach: we have now basic imports and searchs - need more diggin but ready for bigs imports

### sources

- https://www.data.gouv.fr/fr/datasets/cartographie-des-formations-parcoursup-1/#resources
- https://www.data.gouv.fr/fr/datasets/parcoursup-2021-voeux-de-poursuite-detudes-et-de-reorientation-dans-lenseignement-superieur-et-reponses-des-etablissements-1/#resources

### libs

- official: https://github.com/elastic/go-elasticsearch/tree/7.17
- most stars: https://github.com/olivere/elastic

### elasticsearch/kibana install

- https://www.elastic.co/guide/en/kibana/current/docker.html
- https://www.elastic.co/guide/en/elasticsearch/reference/8.2/docker.html

Tried to setup dev env with 8.x.x version. Security configuration are to much a pain just for a test dev. Stay in 7.17 for now:

```bash
sudo sysctl -w vm.max_map_count=262144
podman network create elastic
podman run --name elasticsearch --rm --net elastic --tty --interactive --detach --publish 9200:9200 --publish 9300:9300 --env "discovery.type=single-node" --env ES_JAVA_OPTS="-Xms1g -Xmx1g" docker.elastic.co/elasticsearch/elasticsearch:7.17.3
podman run --name kibana        --rm --net elastic --tty --interactive --detach --publish 5601:5601 docker.elastic.co/kibana/kibana:7.17.3

podman stop elasticsearch kibana
podman network rm -f elastic
```

With compose file:

```bash
podman-compose -f single-node-ek.yml up

podman network rm -f parcoursup-data2elk_elastic
```

```bash
curl -X GET "localhost:9200"
curl -X GET "localhost:9200/_cat/nodes?v=true&pretty"
curl -X GET "localhost:9200/_cat/indices"
curl -X GET "localhost:9200/parcours/_search?pretty" -H 'Content-Type: application/json' -d '{"track_total_hits":true,"query":{"bool":{"must":[{"bool":{"should":[{"match":{"acc_aca_orig":"33"}}],"minimum_should_match":1}}]}}}'
```

```bash
curl -X POST "localhost:5601/api/index_patterns/index_pattern" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d'{"index_pattern":{"title": "parcours*"}}'
```

### previews

![](.images/geo.png)
![](.images/searchpage.png)

### easy search

Use [elasticsearch framework - Search UI](https://github.com/elastic/search-ui) to have a nice and quick search panel.

