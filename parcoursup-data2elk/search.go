package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"github.com/elastic/go-elasticsearch/v7"
)

type esError struct {
	Error errorContent `json:"error"`
}

type errorContent struct {
	RootCause []rootCause `json:"root_cause"`
}

type rootCause struct {
	Type   string `json:"type"`
	Reason string `json:"reason"`
}

type esRes struct {
	Took int  `json:"took"`
	Hits hits `json:"hits"`
}

type hits struct {
	Total totalHits  `json:"total"`
	Hits  []document `json:"hits"`
}

type totalHits struct {
	Value int `json:"value"`
}

type document struct {
	Source docSource `json:"_source"`
}

type docSource struct {
	Session     string `json:"session"`
	ContratEtab string `json:"contrat_etab"`
	CodeUai     string `json:"cod_uai"`
	GeaLibVx    string `json:"g_ea_lib_vx"`
	Dept        string `json:"dep"`
	Libelle     string `json:"lib_comp_voe_ins"`
	Lien        string `json:"lien_form_psup"`
	Voeux       string `json:"voe_tot"`
}

func search(term string) (results []docSource) {
	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	body := fmt.Sprintf(`{"query":{"multi_match":{"type":"phrase","query":"%s"}}}`, term)

	res, err := es.Search(
		es.Search.WithContext(context.Background()),
		es.Search.WithIndex(index),
		es.Search.WithBody(strings.NewReader(body)),
		es.Search.WithTrackTotalHits(true),
		es.Search.WithSize(20),
	)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		var resError esError
		if err := json.NewDecoder(res.Body).Decode(&resError); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		} else {
			log.Fatalf("[%s] %s: %s",
				res.Status(),
				resError.Error.RootCause[0].Type,
				resError.Error.RootCause[0].Reason,
			)
		}
	}

	var dataRes esRes
	if err := json.NewDecoder(res.Body).Decode(&dataRes); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	log.Printf(
		"[%s] search: %s; %d hits; took: %dms",
		res.Status(),
		term,
		dataRes.Hits.Total.Value,
		dataRes.Took,
	)
	for _, hit := range dataRes.Hits.Hits {
		results = append(results, hit.Source)
	}

	return
}
