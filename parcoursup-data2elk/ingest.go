package main

import (
	"bytes"
	"io"
	"log"
	"os"

	"gitlab.com/franckf/reference-opendata/parcoursup-data2elk/bulking"
)

func ingest() {

	// BULK
	/* CSV
	 */
	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatalln("failed to open file:", err)
	}
	defer file.Close()

	err = bulking.Csv(file, index)
	if err != nil {
		log.Fatalln("failed to indexing file:", err)
	}
	// INDEX
	/* CSV
	fileCount, err := os.Open(inputFile)
	if err != nil {
		log.Fatalln("failed to open file:", err)
	}
	defer fileCount.Close()
	lines, _ := countLinesInFile(fileCount)
	if lines >= 64*1024 {
		println("carefull: the file lines reach max buffer of scanner")
	}
	log.Printf("lines: %#+v\n", lines)

	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatalln("failed to open file:", err)
	}
	defer file.Close()

	err = indexing.Csv(file, index)
	if err != nil {
		log.Fatalln("failed to indexing file:", err)
	}
	*/

	/* JSON
	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatalln("failed to open file:", err)
	}
	defer file.Close()

	err = indexing.JsonStructur(file, index)
	if err != nil {
		log.Fatalln("failed to indexing file:", err)
	}
	*/
}

func countLinesInFile(file io.Reader) (count int, err error) {
	buf := make([]byte, 32*1024)
	lineSep := []byte{'\n'}

	for {
		readBytes, err := file.Read(buf)
		if err != nil {
			return count, err
		}
		count += bytes.Count(buf[:readBytes], lineSep)
	}
}
