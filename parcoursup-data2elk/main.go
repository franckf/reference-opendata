package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/elastic/go-elasticsearch/v7"
)

var (
	inputFile string
	reset     bool
)

const index string = "parcours-sup"

func init() {
	flag.StringVar(&inputFile, "f", "", "file to process")
	flag.BoolVar(&reset, "r", false, "delete indices and patterns before indexing")
	flag.Parse()
}

func main() {
	if reset {
		println("deleting patterns and indices")
		deletingDefaultIndexPattern()
		deletingIndex(index)
		ingest()
	}
	voeByDepQuery()
}

func serveSearchAPI() {
	http.Handle("/", http.FileServer(http.Dir("./search-page")))
	http.HandleFunc("/api/v1/search/", searchAPI)
	http.ListenAndServe(":8080", nil)
}

func searchAPI(w http.ResponseWriter, r *http.Request) {
	terms := strings.Split(r.URL.Path, "/")
	result := search(terms[len(terms)-1])
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(result)
	if err != nil {
		fmt.Fprintf(w, "error encoding %#v", err)
	}
}

func deletingDefaultIndexPattern() {
	// we could also retrieve indices patterns in elasticsearch index .kibana
	// but we have only one in this case, so deleting default is enough

	// retrieve default index pattern
	resp, err := http.Get("http://localhost:5601/api/index_patterns/default")
	if err != nil {
		log.Fatalf("error while requesting kibana: %v", err)
	}
	if resp.StatusCode != 200 {
		log.Fatalf("error kibana bad response - %d: %v", resp.StatusCode, err)
	}
	defer resp.Body.Close()
	indexDefault, _ := io.ReadAll(resp.Body)
	indexDefaultID := strings.Split(string(indexDefault), `"`)
	indexID := indexDefaultID[len(indexDefaultID)-2]

	// delete default index pattern
	req, err := http.NewRequest("DELETE", "http://localhost:5601/api/index_patterns/index_pattern/"+indexID, nil)
	if err != nil {
		log.Fatalf("error while requesting kibana: %v", err)
	}
	req.Header.Set("kbn-xsrf", "true")
	client := &http.Client{}
	_, err = client.Do(req)
	if err != nil {
		log.Fatalf("error while requesting kibana: %v", err)
	}
	if resp.StatusCode != 200 {
		log.Fatalf("error kibana bad response - %d: %v", resp.StatusCode, err)
	}

}

func deletingIndex(index string) {
	esClient, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatalf("error creating the client: %v", err)
	}
	res, err := esClient.Indices.Delete([]string{index})
	if err != nil {
		log.Fatalf("error: Indices.Exists: %s", err)
	}
	res.Body.Close()
}
