package mappings

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/elastic/go-elasticsearch/v7"
)

var ParcoursSupFields = `
{
"mappings": {
"properties": {
"session":                        { "type": "integer" },
"contrat_etab":                   { "type": "text" },
"cod_uai":                        { "type": "text" },
"g_ea_lib_vx":                    { "type": "text" },
"dep":                            { "type": "integer" },
"dep_lib":                        { "type": "text" },
"region_etab_aff":                { "type": "text" },
"acad_mies":                      { "type": "text" },
"ville_etab":                     { "type": "text" },
"select_form":                    { "type": "text" },
"fili":                           { "type": "text" },
"lib_comp_voe_ins":               { "type": "text" },
"form_lib_voe_acc":               { "type": "text" },
"fil_lib_voe_acc":                { "type": "text" },
"detail_forma":                   { "type": "text" },
"g_olocalisation_des_formations": { "type": "geo_point" },
"capa_fin":                       { "type": "integer" },
"voe_tot":                        { "type": "integer" },
"voe_tot_f":                      { "type": "integer" },
"nb_voe_pp":                      { "type": "integer" },
"nb_voe_pp_internat":             { "type": "integer" },
"nb_voe_pp_bg":                   { "type": "integer" },
"nb_voe_pp_bg_brs":               { "type": "integer" },
"nb_voe_pp_bt":                   { "type": "integer" },
"nb_voe_pp_bt_brs":               { "type": "integer" },
"nb_voe_pp_bp":                   { "type": "integer" },
"nb_voe_pp_bp_brs":               { "type": "integer" },
"nb_voe_pp_at":                   { "type": "integer" },
"nb_voe_pc":                      { "type": "integer" },
"nb_voe_pc_bg":                   { "type": "integer" },
"nb_voe_pc_bt":                   { "type": "integer" },
"nb_voe_pc_bp":                   { "type": "integer" },
"nb_voe_pc_at":                   { "type": "integer" },
"nb_cla_pp":                      { "type": "integer" },
"nb_cla_pc":                      { "type": "integer" },
"nb_cla_pp_internat":             { "type": "integer" },
"nb_cla_pp_pasinternat":          { "type": "integer" },
"nb_cla_pp_bg":                   { "type": "integer" },
"nb_cla_pp_bg_brs":               { "type": "integer" },
"nb_cla_pp_bt":                   { "type": "integer" },
"nb_cla_pp_bt_brs":               { "type": "integer" },
"nb_cla_pp_bp":                   { "type": "integer" },
"nb_cla_pp_bp_brs":               { "type": "integer" },
"nb_cla_pp_at":                   { "type": "integer" },
"prop_tot":                       { "type": "integer" },
"acc_tot":                        { "type": "integer" },
"acc_tot_f":                      { "type": "integer" },
"acc_pp":                         { "type": "integer" },
"acc_pc":                         { "type": "integer" },
"acc_debutpp":                    { "type": "double" },
"acc_datebac":                    { "type": "double" },
"acc_finpp":                      { "type": "double" },
"acc_internat":                   { "type": "integer" },
"acc_brs":                        { "type": "integer" },
"acc_neobac":                     { "type": "integer" },
"acc_bg":                         { "type": "integer" },
"acc_bt":                         { "type": "integer" },
"acc_bp":                         { "type": "integer" },
"acc_at":                         { "type": "integer" },
"acc_mention_nonrenseignee":      { "type": "integer" },
"acc_sansmention":                { "type": "integer" },
"acc_ab":                         { "type": "integer" },
"acc_b":                          { "type": "integer" },
"acc_tb":                         { "type": "integer" },
"acc_tbf":                        { "type": "double" },
"acc_bg_mention":                 { "type": "integer" },
"acc_bt_mention":                 { "type": "integer" },
"acc_bp_mention":                 { "type": "integer" },
"acc_term":                       { "type": "integer" },
"acc_term_f":                     { "type": "integer" },
"acc_aca_orig":                   { "type": "integer" },
"acc_aca_orig_idf":               { "type": "integer" },
"pct_acc_debutpp":                { "type": "double" },
"pct_acc_datebac":                { "type": "double" },
"pct_acc_finpp":                  { "type": "double" },
"pct_f":                          { "type": "double" },
"pct_aca_orig":                   { "type": "double" },
"pct_aca_orig_idf":               { "type": "double" },
"pct_etab_orig":                  { "type": "double" },
"pct_bours":                      { "type": "double" },
"pct_neobac":                     { "type": "double" },
"pct_mention_nonrenseignee":      { "type": "double" },
"pct_sansmention":                { "type": "double" },
"pct_ab":                         { "type": "double" },
"pct_b":                          { "type": "double" },
"pct_tb":                         { "type": "double" },
"pct_tbf":                        { "type": "double" },
"pct_bg":                         { "type": "double" },
"pct_bg_mention":                 { "type": "double" },
"pct_bt":                         { "type": "double" },
"pct_bt_mention":                 { "type": "double" },
"pct_bp":                         { "type": "double" },
"pct_bp_mention":                 { "type": "double" },
"prop_tot_bg":                    { "type": "double" },
"prop_tot_bg_brs":                { "type": "double" },
"prop_tot_bt":                    { "type": "double" },
"prop_tot_bt_brs":                { "type": "double" },
"prop_tot_bp":                    { "type": "double" },
"prop_tot_bp_brs":                { "type": "double" },
"prop_tot_at":                    { "type": "double" },
"lib_grp1":                       { "type": "text" },
"ran_grp1":                       { "type": "double" },
"lib_grp2":                       { "type": "text" },
"ran_grp2":                       { "type": "double" },
"lib_grp3":                       { "type": "text" },
"ran_grp3":                       { "type": "double" },
"list_com":                       { "type": "text" },
"taux_adm_psup":                  { "type": "double" },
"tri":                            { "type": "text" },
"cod_aff_form":                   { "type": "text" },
"lib_for_voe_ins":                { "type": "text" },
"detail_forma2":                  { "type": "text" },
"lien_form_psup":                 { "type": "text" },
"taux_adm_psup_gen":              { "type": "double" },
"taux_adm_psup_techno":           { "type": "double" },
"taux_adm_psup_pro":              { "type": "double" },
"etablissement_id_paysage":       { "type": "text" },
"composante_id_paysage":          { "type": "text" }
}}}}`

func ParcoursSup(es *elasticsearch.Client, indexName string, mapping string) {
	res, err := es.Indices.Exists([]string{indexName})
	if err != nil {
		log.Fatalf("error: Indices.Exists: %s", err)
	}
	res.Body.Close()
	if res.StatusCode == 404 {
		res, err := es.Indices.Create(
			indexName,
			es.Indices.Create.WithBody(strings.NewReader(mapping)),
		)
		if err != nil {
			log.Fatalf("error: Indices.Create: %s", err)
		}
		if res.IsError() {
			log.Fatalf("error: Indices.Create: %s", res)
		}
	}

}

func CreateIndexPatternKibana(index string) (err error) {
	data := fmt.Sprintf(`{"index_pattern":{"title":"%v*"}}`, index)

	req, err := http.NewRequest("POST", "http://localhost:5601/api/index_patterns/index_pattern", strings.NewReader(data))
	if err != nil {
		err = fmt.Errorf("error while requesting kibana: %s", err)
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("kbn-xsrf", "true")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		err = fmt.Errorf("error while requesting kibana: %s", err)
		return
	}
	if resp.StatusCode != 200 {
		err = fmt.Errorf("error kibana bad response - %d: %s", resp.StatusCode, err)
		return
	}
	return
}
