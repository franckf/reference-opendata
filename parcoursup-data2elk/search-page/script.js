const searchfield = document.getElementById('searchfield')
const cards = document.getElementById('cards')
const searchApiUrl = '/api/v1/search'

searchfield.addEventListener('keypress', e => {
    if (e.key === 'Enter') {
        search(searchfield.value)
    }
})

function search(input) {
    get(input)
}

async function get(input) {
    const res = await fetch(`${searchApiUrl}/${input}`)
    if (!res.ok) {
        const message = `An error has occured: ${res.status}`;
        throw new Error(message);
    }
    const data = await res.json()
    data.forEach(element => {
        insert(element)
    })
}

function insert(data) {
    cards.innerHTML += `
                <div class="card">
                    <div class="card-content">
                        <div class="media">
                            <div class="media-content">
                                <p class="title is-4">${data.g_ea_lib_vx}</p>
                                <p class="subtitle is-6">${data.cod_uai}</p>
                            </div>
                        </div>
                        <div class="content">
                            <p>Département: ${data.dep}</p>
                            <p>${data.lib_comp_voe_ins}</p>
                            <a href="${data.lien_form_psup}">${data.contrat_etab}</a>
                            <br>
                            <time>Session: ${data.session}</time>
                        </div>
                    </div>
                </div>
`
}
