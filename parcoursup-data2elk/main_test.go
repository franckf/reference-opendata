package main

import (
	"io"
	"strings"
	"testing"
)

func Test_countLinesInFile(t *testing.T) {
	type args struct {
		file io.Reader
	}
	tests := []struct {
		name      string
		args      args
		wantCount int
	}{
		{
			name: "one liner",
			args: args{
				file: strings.NewReader("field1;field2\n"),
			},
			wantCount: 1,
		},
		{
			name: "multi liner",
			args: args{
				file: strings.NewReader(`field1;field2;field3
				li1;li2;li3
				li4;li5;li6
				`),
			},
			wantCount: 3,
		},
		{
			name: "multi liner with one line",
			args: args{
				file: strings.NewReader("field1;field2;field3\nli1;li2;li3\n"),
			},
			wantCount: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotCount, _ := countLinesInFile(tt.args.file)
			if gotCount != tt.wantCount {
				t.Errorf("countLinesInFile() = %v, want %v", gotCount, tt.wantCount)
			}
		})
	}
}
