package main

import (
	_ "embed"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/guptarohit/asciigraph"
	chart "github.com/wcharczuk/go-chart"
	"github.com/wcharczuk/go-chart/drawing"
)

func main() {
	startDate := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.Local)

	secPubliqueVienne, _ := os.Open("datas-sort/tableaux-4001-ts-vienne.csv")
	secPubliqueVienneReader, _ := csv.NewReader(secPubliqueVienne).ReadAll()
	secPubVienne := secPubliqueVienneNum(secPubliqueVienneReader)
	createGraph(startDate, secPubVienne, "securite-publique-vienne")

	// columns aren't parse by csv reader for some reason
	secPubliquePoitiers, _ := os.Open("datas-sort/donnee-mini-crimes-et-delits-86194-poitiers.csv")
	secPubliquePoitiersReader, _ := csv.NewReader(secPubliquePoitiers).ReadAll()
	secPubPoitiers := secPubliquePoitiersNum(secPubliquePoitiersReader)
	createGraph(startDate, secPubPoitiers, "securite-publique-poitiers")

	doubleGraph(startDate, secPubVienne, secPubPoitiers, "sec-pub-vienne-poiters")

	// csv reader isn't stable - only take very well formated file
	// use own parsing for the following
	secRoutiereVienne2019, _ := ioutil.ReadFile("datas-sort/accidents-corporels-de-la-circulation-france-2019-2020/vienne-2019.csv")
	secRoutiereVienne2020, _ := ioutil.ReadFile("datas-sort/accidents-corporels-de-la-circulation-france-2019-2020/vienne-2020.csv")
	_, err := ioutil.ReadFile("datas-sort/accidents-corporels-de-la-circulation-vienne-2021.csv")
	if err != nil {
		println("datas for 2021 not available yet - check https://www.data.gouv.fr/fr/datasets/?q=Bases+de+donn%C3%A9es+annuelles+des+accidents+corporels+de+la+circulation+routi%C3%A8re")
	}
	secRoutVienne, secRoutPoitiers := secRoutNum(string(secRoutiereVienne2019), string(secRoutiereVienne2020))

	doubleGraph(startDate, secRoutVienne, secRoutPoitiers, "sec-routiere-vienne-poitiers")

	// duplicate
	// secRoutiereVienne2019Details, _ := os.Open("datas-sort/accidents-corporels-de-la-circulation-vienne-2019.csv")

	// CLI only work on low amplitude
	fmt.Println("-Sécurité-Publique-")
	graphPub := asciigraph.PlotMany([][]float64{secPubVienne, secPubPoitiers}, asciigraph.SeriesColors(asciigraph.Red, asciigraph.Yellow))
	fmt.Println(graphPub)
	fmt.Println("-Sécurité-Routière-")
	graphRou := asciigraph.PlotMany([][]float64{secPubVienne, secPubPoitiers}, asciigraph.SeriesColors(asciigraph.Green, asciigraph.Blue))
	fmt.Println(graphRou)
}

func secRoutNum(d2019 string, d2020 string) (vienne []float64, poitiers []float64) {
	vienne = make([]float64, 39)
	poitiers = make([]float64, 39)

	d2019Lines := strings.Split(d2019, "\n")
	d2020Lines := strings.Split(d2020, "\n")

	for lines := 1; lines < len(d2019Lines)-1; lines++ {
		col := strings.Split(d2019Lines[lines], ";")
		mois, _ := strconv.Atoi(strings.Trim(col[2], `"`))
		ville, _ := strconv.Atoi(strings.Trim(col[7], `"`))
		vienne[mois-1] += 1
		if ville == 86194 {
			poitiers[mois-1] += 1
		}
	}

	for lines := 1; lines < len(d2020Lines)-1; lines++ {
		col := strings.Split(d2020Lines[lines], ";")
		mois, _ := strconv.Atoi(strings.Trim(col[2], `"`))
		ville, _ := strconv.Atoi(strings.Trim(col[7], `"`))
		vienne[mois+12-1] += 1
		if ville == 86194 {
			poitiers[mois+12-1] += 1
		}
	}

	return
}

func secPubliquePoitiersNum(tabs [][]string) (results []float64) {
	var yearsResults = make([]int, 3)
	results = make([]float64, 39)

	for lines := 1; lines < len(tabs); lines++ {
		linesSplit := strings.Split(tabs[lines][0], ";")
		year, _ := strconv.Atoi(linesSplit[1])
		nums, _ := strconv.Atoi(linesSplit[5])
		switch year {
		case 19:
			yearsResults[0] += nums
		case 20:
			yearsResults[1] += nums
		case 21:
			yearsResults[2] += nums
		}
	}

	for i := range results {
		// oh it is ugly ...
		switch {
		case i < 12:
			results[i] = float64(yearsResults[0])
		case i > 12 && i < 25:
			results[i] = float64(yearsResults[1])
		case i > 25:
			results[i] = float64(yearsResults[2])
		}
	}
	return
}

func secPubliqueVienneNum(tabs [][]string) (results []float64) {
	var focusedTabs = make([][]string, len(tabs)-1)
	for i := range focusedTabs {
		focusedTabs[i] = make([]string, 39)
	}

	for lines := 1; lines < len(tabs); lines++ {
		for column := 2; column < 41; column++ {
			focusedTabs[lines-1][column-2] = tabs[lines][column]
		}
	}

	var resultsBadOrder = make([]int, len(focusedTabs[0]))
	for _, l := range focusedTabs {
		for j, c := range l {
			value, _ := strconv.Atoi(c)
			resultsBadOrder[j] += value
		}
	}

	for i := len(resultsBadOrder) - 1; i >= 0; i-- {
		results = append(results, float64(resultsBadOrder[i]))
	}

	return
}

// to regulate values slice must be 39 long
func createGraph(startDate time.Time, values []float64, titre string) {

	values = append(values, 0) // add reference value

	mois := make([]time.Time, len(values))
	for i := range values {
		current := startDate.AddDate(0, i, 0)
		mois[i] = current
	}

	graph := chart.Chart{
		Title:      titre,
		TitleStyle: chart.Style{Show: true},
		Background: chart.Style{
			Padding: chart.Box{
				Top:    30,
				Left:   30,
				Right:  30,
				Bottom: 30,
			},
			FillColor: drawing.ColorFromHex("EEEEEE"),
		},
		XAxis: chart.XAxis{
			Name:      "mois",
			NameStyle: chart.Style{Show: true},
			Style:     chart.Style{Show: true},
		},
		YAxis: chart.YAxis{
			Name:      "valeurs",
			NameStyle: chart.Style{Show: true},
			Style:     chart.Style{Show: true},
		},
		Series: []chart.Series{
			chart.TimeSeries{
				Style: chart.Style{
					Show:        true,
					StrokeWidth: 0.0,
					FillColor:   drawing.ColorFromHex("9BBBBB"),
					StrokeColor: drawing.ColorFromHex("0BBBBB"),
				},
				XValues: mois,
				YValues: values,
			},
		},
	}

	outFile, _ := os.Create(titre + ".png")
	defer outFile.Close()
	graph.Render(chart.PNG, outFile)
}

func doubleGraph(startDate time.Time, values1 []float64, values2 []float64, titre string) {

	values1 = append(values1, 0) // add reference value
	values2 = append(values2, 0) // add reference value

	mois := make([]time.Time, len(values1))
	for i := range values1 {
		current := startDate.AddDate(0, i, 0)
		mois[i] = current
	}

	graph := chart.Chart{
		Title:      titre,
		TitleStyle: chart.Style{Show: true},
		Background: chart.Style{
			Padding: chart.Box{
				Top:    30,
				Left:   30,
				Right:  30,
				Bottom: 30,
			},
			FillColor: drawing.ColorFromHex("EEEEEE"),
		},
		XAxis: chart.XAxis{
			Name:      "mois",
			NameStyle: chart.Style{Show: true},
			Style:     chart.Style{Show: true},
		},
		YAxis: chart.YAxis{
			Name:      "vienne",
			NameStyle: chart.Style{Show: true},
			Style:     chart.Style{Show: true, FontColor: drawing.ColorFromHex("0BBBBB")},
		},
		YAxisSecondary: chart.YAxis{
			Name:      "poitiers",
			NameStyle: chart.Style{Show: true},
			Style:     chart.Style{Show: true, FontColor: drawing.ColorFromHex("BBBB0B")},
		},
		Series: []chart.Series{
			chart.TimeSeries{
				Style: chart.Style{
					Show:        true,
					StrokeWidth: 1.0,
					StrokeColor: drawing.ColorFromHex("0BBBBB"),
				},
				XValues: mois,
				YValues: values1,
			},
			chart.TimeSeries{
				Style: chart.Style{
					Show:        true,
					StrokeWidth: 1.0,
					StrokeColor: drawing.ColorFromHex("BBBB0B"),
				},
				YAxis:   chart.YAxisSecondary,
				XValues: mois,
				YValues: values2,
			},
		},
	}

	outFile, _ := os.Create(titre + ".png")
	defer outFile.Close()
	graph.Render(chart.PNG, outFile)
}
