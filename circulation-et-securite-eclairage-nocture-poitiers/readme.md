# Les conséquences de l'extinction de l'éclairage nocturne à Poitiers (86000) sur les accidents de la route et les crimes et délits.

Depuis le 11 octobre 2021, les éclairages publiques à Poitiers sont éteints de minuit à 5 heures.

Au-delà des bénéfices sur l'énergie, la bio-diversité et les cycles de sommeil des citoyens, quelles sont les conséquences sur la sécurité routière et publique ?

A partir des données publiques (open data), vérifions.

### Décisions de la mairie, sources:
- https://www.lanouvellerepublique.fr/chatellerault/extinction-des-feux-la-nuit-l-eclairage-chatelleraudais
- https://www.lanouvellerepublique.fr/poitiers/poitiers-le-debat-sur-l-eclairage-public-n-est-pas-clos
- https://www.lanouvellerepublique.fr/poitiers/poitiers-la-ville-va-vivre-des-nuits-noires
- https://www.lanouvellerepublique.fr/poitiers/poitiers-l-extinction-de-l-eclairage-nocturne-partage-les-citoyens

- https://lasocietemoderne.com/poitiers-va-etendre-lextinction-nocturne-de-leclairage-public/
- https://france3-regions.francetvinfo.fr/nouvelle-aquitaine/vienne/poitiers/poitiers-va-etendre-l-extinction-nocturne-de-l-eclairage-public-2287696.html

- https://www.poitiers.fr/c__0_0_Actualite_39933__0__L_extinction_de_l_eclairage_public_repoussee_a_2h_du_matin.html
- https://www.poitiers.fr/c__0_0_Dossier_39478__0__Experimentation_de_l_eclairage_public_responsable_a_Poitiers.html
- https://www.poitiers.fr/c__396_1687__Extinction_de_l_eclairage_public.html
- https://grandpoitiers.opendatasoft.com/explore/dataset/evaluation-extinction-eclairage-public-economie-financiere/information/
- https://grandpoitiers.opendatasoft.com/explore/dataset/evaluation-extinction_eclairage_public_questionnaires/information/
- https://siggrandpoitiers.maps.arcgis.com/apps/dashboards/271f5b6136db4478a2125886d3af3bf9

### Données publiques, sources:
- https://data.grandpoitiers.fr/explore/dataset/accidents-corporels-de-la-circulation-millesime/information/?disjunctive.com_name&disjunctive.dep_code&disjunctive.dep_name&disjunctive.epci_code&disjunctive.epci_name&disjunctive.reg_code&disjunctive.reg_name&disjunctive.com_code
- https://www.data.gouv.fr/fr/datasets/bases-communale-et-departementale-des-principaux-indicateurs-des-crimes-et-delits-enregistres-par-la-police-et-la-gendarmerie-nationales/
- https://data.grandpoitiers.fr/explore/dataset/espace-public-eclairage-public-grand-poitiers-donnees-metiers/export/
- https://data.grandpoitiers.fr/explore/dataset/decoupages-administratifs-quartiers-poitiers-grand-poitiers-donnees-de-reference/

### Tri des données

On réduit le champs dans la masse des données récupérées.
On se concentre sur la zone du Grand Poitiers ou sur la Vienne, si le premier n'est pas accessible, car on a parfois les données pour toute la France.
On se concentre également sur les années 2019, 2020, 2021 et 2022, et on favorisera une comparaison avec les éléments de l'année 2019, les autres années étant plus ou moins affectées par la COVID-19 et les confinements successifs.

Concernant les données sur la sécurité publique, on a :
- donnee-mini-crimes-et-delits-86194-poitiers.csv
- tableaux-4001-gn-vienne.csv (gendarmerie)
- tableaux-4001-pn-vienne.csv (police)
- tableaux-4001-ts-vienne.csv (total)

On a bien toutes les données nécéssaires sur la Vienne (par mois) et Poitiers (par année), même un peu détaillées sur les classes des infractions commises.

Concernant les données sur la sécurité routière, on a :
- accidents-corporels-de-la-circulation-vienne-2019.csv
- accidents-corporels-de-la-circulation-france-2019-2020

On est limité à l'année 2019 en plus ancien. On peut éventuellement récupérer les données 2020 au niveau National https://www.data.gouv.fr/fr/datasets/bases-de-donnees-annuelles-des-accidents-corporels-de-la-circulation-routiere-annees-de-2005-a-2020/, pour ensuite en extraire les éléments de la Vienne.

Mais les données 2021 sont toujours en cours de consolidations (cf onisr https://www.data.gouv.fr/fr/datasets/bases-de-donnees-annuelles-des-accidents-corporels-de-la-circulation-routiere-annees-de-2005-a-2020/#discussion-622759395fba54135a369ae6). Les données temporaires de l'onisr ne permettent pas d'extraire les données de la Vienne pour 2021.

On va préparer les données 2019 et 2020 en attendant que les données 2021 soient disponibles. Mais cela va ralentir l'analyse.

### Résultats

![](sec-pub-vienne-poiters.png)
![](sec-routiere-vienne-poitiers.png)
![](securite-publique-poitiers.png)
![](securite-publique-vienne.png)
