module gitlab.com/franckf/reference-opendata/circulation-et-securite-eclairage-nocture-poitiers

go 1.18

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/guptarohit/asciigraph v0.5.5 // indirect
	github.com/wcharczuk/go-chart v2.0.1+incompatible // indirect
	golang.org/x/image v0.0.0-20220413100746-70e8d0d3baa9 // indirect
)
