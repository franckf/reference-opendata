# reference opendata

Explore opendata to help the community and the environment as citizen service.

### datas sources

- [CERN](http://opendata.cern.ch)
- [wikipedia](https://en.wikipedia.org/wiki/Wikipedia:Database_download)
- [opendatasoft](https://data.opendatasoft.com/pages/home/)
- [insee-rdf](http://rdf.insee.fr/index.html)
- [data.gouv.fr](https://www.data.gouv.fr/fr/)
- [data.gov](https://catalog.data.gov/dataset)
- [api.gouv.fr](https://api.gouv.fr/rechercher-api)

### file format

- [SPARQL](https://fr.wikipedia.org/wiki/SPARQL)
- [Resource Description Framework](https://fr.wikipedia.org/wiki/Resource_Description_Framework)
- [dBase](https://www.insee.fr/fr/information/2410904)
- [beyond](https://www.insee.fr/fr/information/2410868)

### deal with xls files in go

- use `cgo` and `libxls` to deal with xls files
- `catdoc`
- `xls2csv`
- `ssconvert book.xlsx file.csv` (gnumeric)
- https://github.com/northbright/xls2csv-go
- https://github.com/extrame/xls
- https://github.com/shakinm/xlsReader

### faster xml

- https://eli.thegreenplace.net/2019/faster-xml-stream-processing-in-go/

### visualizations

- Kibana 
- draw image with standard library
- https://github.com/wcharczuk/go-chart

### projects

- [enwiki-20210620-abstract.xml - simple wikipedia parser as a test run](https://gitlab.com/franckf/enwiki-20210620-abstract.xml)
- [parcoursup - imports differents format to elk containers](https://gitlab.com/franckf/reference-opendata/parcoursup-data2elk)
- [Les conséquences de l'extinction de l'éclairage nocturne à Poitiers (86000) sur les accidents de la route et les crimes et délits.](https://gitlab.com/franckf/reference-opendata/-/tree/main/circulation-et-securite-eclairage-nocture-poitiers)

